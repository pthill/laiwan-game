package com.java80.littlegame.service.game.desk;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java80.littlegame.common.base.GameUser;
import com.java80.littlegame.common.base.VertxMessageHelper;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.cluster.ClusterMessage;
import com.java80.littlegame.common.message.proto.cluster.DeskEndMessage;
import com.java80.littlegame.service.game.GameConfig;

public abstract class Desktop {
	protected DeskSetting setting;
	protected boolean DesktopRun;
	final transient static Logger log = LoggerFactory.getLogger(Desktop.class);

	public Desktop(DeskSetting setting) {
		super();
		this.setting = setting;
		this.DesktopRun = true;
	}

	/**
	 * 启动一局游戏
	 */
	public abstract void onStart();

	/**
	 * 接收客户端消息
	 */
	public abstract void onMessage(long userId, BaseMsg msg);

	/**
	 * 系统消息
	 */
	public abstract void onClusterMessage(ClusterMessage msg);

	/**
	 * 给局内玩家发消息
	 * 
	 * @param msg
	 */
	public void broadcastMessage(BaseMsg msg) {
		List<Long> playerIds = setting.getPlayerIds();
		for (Long id : playerIds) {
			sendMessage(id, msg);
		}
	}

	/**
	 * 给局内玩家发消息
	 * 
	 */
	public void sendMessage(long userId, BaseMsg msg) {
		GameUser gameUser = GameUser.getGameUser(userId);
		msg.setSessionId(null);
		msg.setRecUserId(userId);
		log.debug("send msg -> {},{}", gameUser, msg);
		VertxMessageHelper.sendMessageToGateWay(gameUser, msg);
	}

	public void endDesk() {
		DeskEndMessage end = new DeskEndMessage();
		int roomId = setting.getRoomId();
		end.setRoomId(roomId);
		VertxMessageHelper.sendMessageToService(GameConfig.getQueueName(), end.toString());
	}

	public DeskSetting getSetting() {
		return setting;
	}

	public void setSetting(DeskSetting setting) {
		this.setting = setting;
	}
}
