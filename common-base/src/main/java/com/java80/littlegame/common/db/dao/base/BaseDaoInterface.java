package com.java80.littlegame.common.db.dao.base;

import java.sql.SQLException;

public interface BaseDaoInterface<T> {
	public void insert(T t) throws SQLException;

	public void delete(long id) throws SQLException;

	public void update(T t) throws SQLException;

	public T get(long id) throws Exception;

}
