package com.java80.littlegame.common.cache;

public abstract class CacheObj {
	public abstract String getKeyName();

	public void save() {
		CacheService.saveObject(makeKeyName(getKeyName()), this);
	}

	public void remove() {
		CacheService.remove(makeKeyName(getKeyName()));
	}

	public static String makeKeyName(String keyName) {
		return CacheService.CACHEOBJPROFIX + keyName;
	}
	/**
	 * map存储一组数据 map删除其中某个 map修改其中的一组
	 * 
	 */
}
