package com.java80.littlegame.common.message.proto;

import com.java80.littlegame.common.utils.GsonUtil;

public abstract class BaseMsg {
	protected long senderUserId;
	protected long recUserId;
	protected int type;
	protected int code;
	protected String sessionId;

	public BaseMsg() {
		super();
		this.code = getCode();
		this.type = getType();
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public abstract int getType();

	public void setType(int type) {
		this.type = getType();
	}

	public abstract int getCode();

	public void setCode(int code) {
		this.code = getCode();
	}

	public long getSenderUserId() {
		return senderUserId;
	}

	public void setSenderUserId(long senderUserId) {
		this.senderUserId = senderUserId;
	}

	public long getRecUserId() {
		return recUserId;
	}

	public void setRecUserId(long recUserId) {
		this.recUserId = recUserId;
	}

	@Override
	public String toString() {
		return GsonUtil.parseObject(this);
	}

}
