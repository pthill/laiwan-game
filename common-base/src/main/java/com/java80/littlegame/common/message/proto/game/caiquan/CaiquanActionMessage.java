package com.java80.littlegame.common.message.proto.game.caiquan;

import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoList;

/**
 * 
 * 猜拳动作 server<->client 客户端收到消息就开始动作，服务端收到表示客户端出拳了
 *
 */
public class CaiquanActionMessage extends BaseMsg {
	private byte actionId;// 出拳动作
	private byte result;// 出拳结果 石头剪刀布

	public byte getActionId() {
		return actionId;
	}

	public void setActionId(byte actionId) {
		this.actionId = actionId;
	}

	public byte getResult() {
		return result;
	}

	public void setResult(byte result) {
		this.result = result;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_CAIQUANACTION;
	}

}
