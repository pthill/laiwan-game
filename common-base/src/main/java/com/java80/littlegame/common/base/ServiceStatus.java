package com.java80.littlegame.common.base;

import java.io.Serializable;

public class ServiceStatus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7531065839428959293L;
	private int serviceType;
	private String instanceName;
	private String serviceId;
	private String serviceQueueName;

	public int getServiceType() {
		return serviceType;
	}

	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceQueueName() {
		return serviceQueueName;
	}

	public void setServiceQueueName(String serviceQueueName) {
		this.serviceQueueName = serviceQueueName;
	}

	@Override
	public String toString() {
		return "ServiceStatus [serviceId=" + serviceId + "]";
	}

}
