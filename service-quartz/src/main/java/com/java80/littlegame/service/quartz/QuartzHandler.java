package com.java80.littlegame.service.quartz;

import com.java80.littlegame.common.base.BaseHandler;
import com.java80.littlegame.common.message.proto.BaseMsg;
import com.java80.littlegame.common.message.proto.ProtoHelper;
import com.java80.littlegame.common.message.proto.timer.TimerMessage;

public class QuartzHandler extends BaseHandler {

	public void onMessage(BaseMsg msg) {
		switch (msg.getCode()) {
		default:
			break;
		}
		if (msg instanceof TimerMessage) {

			CronTask ct = new CronTask(((TimerMessage) msg).getDelay(), msg.toString(),
					((TimerMessage) msg).getTargetQueue());
			CronJobTools.addSystemTask(ct);
		}
	}

	@Override
	public Object onReceive(String obj) {
		onMessage(ProtoHelper.parseJSON(obj.toString()));
		return null;
	}

}
